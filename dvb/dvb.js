module.exports = function(RED) {
    function dvbNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
            const dvb = require("dvbjs")
	    var foo;
	    dvb.findStop(msg.payload).then((data) => {
		    var id = data[0].id;
		    var node = this;
		    dvb.monitor(id, node.timeOffset, node.numberResults).then((data) => {
		    	msg.payload = data;
			node.send(msg);
		    });
	   });
    	});
    }
    RED.nodes.registerType("dvb",dvbNode);
}

